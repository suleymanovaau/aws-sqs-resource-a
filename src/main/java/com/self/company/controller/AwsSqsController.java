package com.self.company.controller;

import com.self.company.service.SqsPublisher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("sqs/")
public class AwsSqsController {

    final SqsPublisher sqsPublisher;

    public AwsSqsController(SqsPublisher sqsPublisher) {
        this.sqsPublisher = sqsPublisher;
    }

    @PostMapping("message")
    public void sendMessage(@RequestParam String message){
        sqsPublisher.send(message);
    }


}
