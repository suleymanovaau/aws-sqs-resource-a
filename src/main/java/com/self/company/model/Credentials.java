package com.self.company.model;

import lombok.Data;

@Data
public class Credentials {
    private String accessKey;
    private String secretKey;
}
