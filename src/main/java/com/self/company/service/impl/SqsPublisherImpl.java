package com.self.company.service.impl;

import com.self.company.model.AwsProperties;
import com.self.company.service.SqsPublisher;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class SqsPublisherImpl implements SqsPublisher {

    private Logger logger =  LoggerFactory.getLogger(this.getClass());

    private final QueueMessagingTemplate queueMessagingTemplate;

    private final AwsProperties awsProperties;


    @Override
    public void send(String message) {
        Map<String, Object> map = new HashMap<>();
        map.put("message-group-id", UUID.randomUUID().toString());
        map.put("message-deduplication-id", UUID.randomUUID().toString());
        queueMessagingTemplate.convertAndSend(awsProperties.getQueue(),message,map);
//        Message<?> message1 = queueMessagingTemplate.receive("my-aws-queue.fifo");
    }




}
