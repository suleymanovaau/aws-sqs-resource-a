package com.self.company.service;

public interface SqsPublisher {
    void send(String message);
}
