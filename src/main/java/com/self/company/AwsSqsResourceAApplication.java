package com.self.company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
public class AwsSqsResourceAApplication {

    public static void main(String[] args) {
        SpringApplication.run(AwsSqsResourceAApplication.class, args);
    }

}
